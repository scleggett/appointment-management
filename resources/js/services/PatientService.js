import http from "../http-common";

class PatientService {
    getAll() {
        return http.get("/patients");
    }
    findByName(name) {
        return http.get(`/patients?name=${name}`);
    }
}
export default new PatientService();
