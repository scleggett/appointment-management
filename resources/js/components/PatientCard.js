const PatientCard = (props) => {
    return (
        <div style={{"display": "flex", "padding": "10px"}} className="patientCard">
            <div style={{
                     "backgroundColor": '#' + Math.floor(Math.random()*16777215).toString(16).padStart(6, '0'),
                     "flex": "0 0 50px",
                     "height": "50px",
                     "marginRight": "10px",
                     "borderRadius": "50%",
                        "display": "flex",
                        "alignItems": "center",
                        "justifyContent": "center"
                 }}>
                {props.patient.first_name[0]}{props.patient.last_name[0]}
            </div>
            <div>
                <div style={{"fontWeight": "800"}}>
                    {props.patient.first_name} {props.patient.last_name}
                </div>
                <div style={{"opacity": "0.5"}}>
                    {props.patient.date_of_birth}
                </div>
                <div>
                    {props.patient.phone}
                </div>
            </div>
        </div>
    )
}

export default PatientCard;
