import React, { useState, useEffect } from "react";
import PatientDataService from "../services/PatientService";
import ReactDOM from "react-dom";
import PatientCard from './PatientCard'

const PatientsList = () => {
    const [patients, setPatients] = useState([]);
    const [currentPatient, setCurrentPatient] = useState(null);
    const [currentIndex, setCurrentIndex] = useState(-1);
    const [searchName, setSearchName] = useState("");

    useEffect(() => {
        retrievePatients();
    }, []);

    const onChangeSearchName = e => {
        const searchName = e.target.value;
        setSearchName(searchName);
    };

    const retrievePatients = () => {
        PatientDataService.getAll()
            .then(response => {
                setPatients(response.data.data);
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    };

    const setActivePatient = (patient, index) => {
        setCurrentPatient(patient);
        setCurrentIndex(index);
    };

    const clearActivePatient = () => {
        setCurrentPatient(null);
        setCurrentIndex(null);
    }

    const findByName = () => {
        PatientDataService.findByName(searchName)
            .then(response => {
                setPatients(response.data.data);
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    };

    return (
        <div id="page">
            <div className="list" style={ currentPatient && {filter: "blur(4px)", opacity: "0.5"}}>
                <div id="patientSearch">
                    <div className="input-group mb-3">
                        <input
                            type="text"
                            className="form-control"
                            placeholder="Search by name"
                            value={searchName}
                            onChange={onChangeSearchName}
                        />
                        <div className="input-group-append">
                            <button
                                className="btn btn-outline-secondary"
                                type="button"
                                onClick={findByName}
                            >
                                Search
                            </button>
                        </div>
                    </div>
                </div>
                <div id="patientList">
                    <h4>Patients List</h4>

                    <ul className="list-unstyled row">
                        {patients && patients.map((patient, index) => (
                            <li
                                className={
                                    "col-xl-3" + (index === currentIndex ? "active" : "")
                                }
                                onClick={() => setActivePatient(patient, index)}
                                key={index}
                            >
                                <PatientCard patient={patient}></PatientCard>
                            </li>
                        ))}
                    </ul>
                </div>
            </div>
            {currentPatient && (
                <div style={{
                    position: "absolute",
                    width: "100%",
                    height: "100%",
                    display: "flex",
                    top: 0,
                    alignItems: "center",
                    bottom: 0,
                    right: 0,
                    left: 0,
                    justifyContent: "center",
                    backgroundColor: "rgb(0 0 0 / 8%)",
                }}>
                    <div id="patientDetail" style={{width:"300px", backgroundColor: "white", maxHeight: "400px"}}>
                        <div id="patientTopBar" style={{"borderBottom": "1px solid #f2f2f2", display: "flex"}}>
                            <div style={{flexGrow: 1}}>
                                <PatientCard patient={currentPatient}></PatientCard>
                            </div>
                            <a type="button"
                                    style={{"flex": "0 0 20px", "marginLeft": "5px", "height": "25px", "textDecoration": "none", "color": "#CCC"}}
                                    onClick={() => clearActivePatient()}
                            >
                                <span aria-hidden="true">&times;</span>
                            </a>
                        </div>
                        <ul id="appointmentList" className="list-unstyled" style={{"backgroundColor": "#f9f9f9", "borderBottom": "#f0eeee", maxHeight: "400px", overflow: "scroll"}}>
                            {currentPatient.appointments.map((appointment, index) => (
                                <li style={{"padding": "10px"}} id="appointment" key={index}>
                                    <div style={{"marginLeft": "60px"}} >
                                        <strong>{appointment.appointment_start}</strong>
                                        <br/>
                                        {appointment.type}
                                    </div>
                                </li>
                            ))}
                        </ul>
                    </div>
                </div>
            )}
        </div>
    );
};

export default PatientsList;
// DOM element
if (document.getElementById('patients')) {
    ReactDOM.render(<PatientsList />, document.getElementById('patients'));
}

