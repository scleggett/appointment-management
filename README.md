# Appointment Management
After cloning this repo, cd into the directory and run:
`composer install`
`npm install`
## Database
### SQLite
To run the project you can quickly setup a database using sqlite.
Clone the .env.example file. Replace the DB options with the following:
`DB_CONNECTION=sqlite`
Create an empty file to hold the database file by running
`touch database/database.sqlite`

## Running for the first time
To initialize the project for the first time run: 
`php artisan migrate`
`php artisan db:seed --class=PatientSeeder`
`php artisan db:seed --class=AppointmentSeeder`

## Running the project
Easily run using:
`php artisan serve`
Edit javascript files using:
`npm run watch`

## Run tests
### E2E Test
Launch cypress and click on the tests to run:
`npx cypress open`
Run laravel tests via
`php artisan test`
### 

## Main Improvements
- [-] Add Dialogs
- [-] Make Match mockups
   - the search bar was not on the mockup - added
   - the font and spacing might be slightly different (could be customized via less)
   - mockups use 5 column grid - standard is 12 so opted for 6 column.
- [x] Add appointments endpoint
- [x] Add appointments and service layer (ended up adding as repo layer)
    - services in laravel seem to get results mixed with Providers so I couldn't find great guides for using them in more recent versions
- [-] Add tests for service layer
- [ ] Add request validations
     
## Additional Improvements
- add better seeder file so you don't need to explicitly state class
- test the readme to make sure no errors etc :-)
- responsive styles
- time format for appointmet front end
- better react bootstrap integration?
- fix BaseController so you don't have data.data
- pagination
- filter out previous appointments?
- limit resourceful routing? (only need GET for now)
- use mocks for controller smoke tests?
- consider breaking up service/repository files (sometimes more granular ones are nice)
- pass in array of filter options to the filter function on AppointmentRepo for flexible future filtering
- explore repository pattern vs services more...it seems like we'd actaully want to be using a service but the examples I found hit repos directly so I went with it.
    my understanding of services vs repos is:
    - services are for business logic, so more create/update logic?
    - repositories are just a data access layer that goes over the database
- not sure if I did associations right on factory
- make front end use appointments endpoint!!!
- would like to have re-used utilities even more instead of adding custom css to front end
- explore more laravel test patterns (i,e, nested tests, seeding, multiple assertions...)
- I'm not sure about my constructor for my repository
- Use consistent color for patient (generate via initals or something)
- Really not sure about these inline styles (hard to copy from dev tools and weird to write and not sure about responsivity). 
    Might be nice for organization of non-utilities though since not class based?
- always have to re-familiarize with flexbox...
- used ids as labels but probably should break into more components
- dont use px directly? more variables for spacing...
- definitely need to get up to speed on current best practices in react...
- front end linter
- is dialog a one time component or a shared component?
- layout gets skewed when you click dialog
- exact px measurements are probably off.. need my ruler extension...
- could have probably used more sass
- not sure font? used system default (omggg)
- wish I hadn't used bootstrap since this CSS is so generic and had just used a css reset or something...
- better shorthand css
- better way to do dialog overlay...
- dialog is generally janky... but got to call it...
- small problems with factory (date time not supported? need to import another package?)
