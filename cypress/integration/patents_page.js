describe('Patients Page', () => {
  before(() => {
    cy.visit('http://127.0.0.1:8000')
  })
  it('Displays header', () => {
    cy.get('h4')
      .should('contain', 'Patients List')
  })
  it('Loads patients from the database', () => {
    cy.get('#patients')
      .should('contain', 'Julianne Simonis')
  })
  it('Loads patients details on click', () => {
    cy.get('#patients').contains('Julianne Simonis')
      .click()

    cy.get('#patientDetail').should('contain', 'Short Visit')
  })
  it('Searches by name', () => {
      cy.get('#patientSearch input').type('Mo He')
      cy.get('#patientSearch button').click()

      cy.get('#patients')
         .should('contain', 'Molly Hessel')
         .should('not.contain', 'Julianne Simonis')
  })
})
