<?php

namespace Database\Seeders;

use App\Models\Patient;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AppointmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csv = array_map('str_getcsv', file('database/seeders/data/appointments.csv'));
        array_walk($csv, function(&$a) use ($csv) {
            $a = array_combine($csv[0], $a);
        });
        array_shift($csv);
        foreach ($csv as &$row) {
            $name_pieces = explode(' ', $row['Patient Name']);
            $patient = Patient::where('first_name', $name_pieces[0])
                ->where('last_name', $name_pieces[1])
                ->first();

            $date_time_string = strval($row['Start Date']).' '.$row['Start Time'];
            DB::table('appointments')->insert([
                'patient_id' => $patient->id,
                'appointment_start' => Carbon::createFromFormat('Y-m-d g:i a', $date_time_string, 'America/Los_Angeles'),
                'type' => $row['Appointment Type']
            ]);
        }
    }
}
