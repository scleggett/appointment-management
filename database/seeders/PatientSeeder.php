<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class PatientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csv = array_map('str_getcsv', file('database/seeders/data/patients.csv'));
        array_walk($csv, function(&$a) use ($csv) {
          $a = array_combine($csv[0], $a);
        });
        array_shift($csv);
        foreach ($csv as &$row) {
            DB::table('patients')->insert([
                'first_name' => $row['First Name'],
                'last_name' => $row['Last Name'],
                'date_of_birth' => $row['Date of Birth'],
                'phone' => $row['Phone Number'],
            ]);
        }
    }
}
