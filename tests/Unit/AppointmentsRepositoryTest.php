<?php

namespace Tests;

use App\Models\Appointment;
use App\Repository\AppointmentRepositoryInterface;
use App\Repository\Eloquent\AppointmentRepository;
use Tests\TestCase;

class AppointmentsRepositoryTest extends TestCase
{
    /**
     * @var AppointmentRepositoryInterface
     */
    private $appointmentRepository;

    public function setUp(): void
    {
        parent::setUp();
        $this->appointmentRepository = app(AppointmentRepository::class);
//        $this->appointmentRepository = new AppointmentRepository(new Appointment()); //todo: this is weird to me..
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_filter()
    {
        $patient_id = 1;
        $other_patient_id = 2;
        $patient_appointments = Appointment::factory()->count(2)->create([
            'patient_id' => $patient_id,
        ]);
        $other_patient_appointments = Appointment::factory()->create([
            'patient_id' => $other_patient_id,
        ]);
        $results = $this->appointmentRepository->filter($patient_id);
        $this->assertTrue($results->contains($patient_appointments[0]));
        $this->assertTrue($results->contains($patient_appointments[1]));
        $this->assertFalse($results->contains($other_patient_appointments));
    }
}
