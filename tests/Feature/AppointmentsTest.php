<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AppointmentTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_patient_appointments_index()
    {
        $response = $this->get('/api/patients/1/appointments');

        $response->assertStatus(200);
    }
}
