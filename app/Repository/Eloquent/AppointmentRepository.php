<?php

namespace App\Repository\Eloquent;

use App\Models\Appointment;
use App\Repository\AppointmentRepositoryInterface;
use Illuminate\Support\Collection;

class AppointmentRepository extends BaseRepository implements AppointmentRepositoryInterface
{

   /**
    * AppointmentRepository constructor.
    *
    * @param Appointment $model
    */
   public function __construct(Appointment $model)
   {
       parent::__construct($model);
   }

   /**
    * @return Collection
    */
   public function filter($patient_id): Collection
   {
       return $this->model->where('patient_id', $patient_id)->get();
   }
}
