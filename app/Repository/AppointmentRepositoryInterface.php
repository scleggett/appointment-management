<?php
namespace App\Repository;

use Illuminate\Support\Collection;

interface AppointmentRepositoryInterface
{
   public function filter($patient_id): Collection;
}
