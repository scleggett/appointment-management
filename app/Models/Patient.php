<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    use HasFactory;

    /**
     * Get the comments for the blog post.
     */
    public function appointments()
    {
        return $this->hasMany(Appointment::class);
    }
}
