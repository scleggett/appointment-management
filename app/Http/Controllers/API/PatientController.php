<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Models\Patient;
use App\Http\Resources\PatientResource;

class PatientController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $patients_query = Patient::with(['appointments' => function ($query) {
            $query->orderBy('created_at', 'desc');
        }]);
        if ($request->input('name')){
            $name_pieces = explode(' ', $request->input('name'));
            if (count($name_pieces) > 1){
                $patients_query
                    ->where('first_name','like', '%'.$name_pieces[0].'%')
                    ->where('last_name', 'like', '%'.$name_pieces[1].'%');
            }else{
                $patients_query
                    ->where('first_name','like', '%'.$name_pieces[0].'%')
                    ->orWhere('last_name', 'like', '%'.$name_pieces[0].'%');
            }
        }
        $patients = $patients_query->get();
        return $this->sendResponse(PatientResource::collection($patients), 'Patients retrieved successfully.');
    }
}
