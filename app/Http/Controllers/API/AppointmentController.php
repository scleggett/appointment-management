<?php

namespace App\Http\Controllers\API;

use App\Repository\AppointmentRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Resources\AppointmentResource;

class AppointmentController extends BaseController
{
   private $appointmentRepository;

   public function __construct(AppointmentRepositoryInterface $appointmentRepository)
   {
       $this->appointmentRepository = $appointmentRepository;
   }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $patient_id)
    {
        $results = $this->appointmentRepository->filter($patient_id);
        return $this->sendResponse(AppointmentResource::collection($results), 'Appointments retrieved successfully.');
    }
}
